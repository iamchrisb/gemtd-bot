FROM openjdk:8-jre-alpine

COPY build/libs/gemtd-bot-all.jar /gemtd-bot.jar

CMD ["/usr/bin/java", "-jar", "gemtd-bot.jar"]

