package de.cbl.home;

import com.pengrad.telegrambot.Callback;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import de.cbl.home.telegram.TelegramHelper;
import java.io.IOException;
import java.util.TimerTask;
import net.dean.jraw.RedditClient;
import net.dean.jraw.models.Listing;
import net.dean.jraw.models.Submission;
import net.dean.jraw.models.SubredditSort;
import net.dean.jraw.pagination.DefaultPaginator;
import net.dean.jraw.references.SubredditReference;

public class GemTdShopTask extends TimerTask {

  private static final String NEW_LINE = "\n";
  private static final int REDDIT_POST_LIMIT = 5;

  private String latestSubmissionUniqueId;
  private TelegramBot telegramBot;
  private RedditClient redditClient;

  public GemTdShopTask(TelegramBot telegramBot, RedditClient redditClient) {
    this.telegramBot = telegramBot;
    this.redditClient = redditClient;
  }

  @Override
  public void run() {
    Listing<Submission> submissions = getAllSubmissions();

    if (submissions == null || submissions.isEmpty()) {
      //TODO @cbl logger / monitoring
      return;
    }

    Submission latestSubmission = getLatestShopSubmission(submissions);

    if (latestSubmission == null) {
      return;
    }

    String currentSubmissionUniqueId = latestSubmission.getUniqueId();

    if (currentSubmissionUniqueId.equals(this.latestSubmissionUniqueId)) {
      return;
    }

    String latestStoreUpdateMessage = new StringBuilder()
        .append(latestSubmission.getCreated())
        .append(NEW_LINE)
        .append(TelegramHelper
            .link(Constants.LINK_TEXT, Constants.REDDIT_DOMAIN + latestSubmission.getPermalink()))
        .append(NEW_LINE)
        .append(TelegramHelper.bold(Constants.DESCRITPION_TEXT))
        .append(NEW_LINE)
        .append(TelegramHelper.code(latestSubmission.getSelfText()))
        .toString();

    System.out.println(latestStoreUpdateMessage);
    broadcast(latestStoreUpdateMessage, currentSubmissionUniqueId);
  }

  private Listing<Submission> getAllSubmissions() {
    SubredditReference gemTdSubReddit = this.redditClient
        .subreddit(Constants.GEM_TD_SUBREDDIT_NAME);
    DefaultPaginator<Submission> posts = gemTdSubReddit.posts()
        .sorting(SubredditSort.NEW)
        .limit(REDDIT_POST_LIMIT).build();

    return posts.next();
  }

  private Submission getLatestShopSubmission(Listing<Submission> submissions) {
    for (Submission submission : submissions) {
      String trimmedTitle = submission.getTitle().toLowerCase().trim();
      if (trimmedTitle.startsWith(Constants.REDDIT_SHOP_PREFIX)) {
        return submission;
      }
    }
    return null;
  }

  private void broadcast(String latestStoreUpdateMessage,
      String currentSubmissionUniqueId) {
    SendMessage request = new SendMessage(Constants.TELEGRAM_CHANNEL_NAME,
        latestStoreUpdateMessage)
        .parseMode(ParseMode.Markdown).disableWebPagePreview(true);

    this.telegramBot.execute(request, new Callback<>() {
      @Override
      public void onResponse(SendMessage request, SendResponse response) {
        if (response == null) {
          //TODO @cbl logger / monitoring
          System.err.println("Something went wrong");
        }

        GemTdShopTask.this.latestSubmissionUniqueId = currentSubmissionUniqueId;
        System.out.println(response);
      }

      @Override
      public void onFailure(SendMessage request, IOException e) {
        //TODO @cbl logger / monitoring

      }
    });
  }
}
