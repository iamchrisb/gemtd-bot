package de.cbl.home;

public class Constants {

  public static final String USER_AGENT_PLATTFORM = "gemtd";
  public static final String USER_AGENT_APP_ID = "de.cbl.home";
  public static final String USER_AGENT_VERSION = "v0.1";
  public static final String REDDIT_USERNAME = "cbcream";
  public static final String GEM_TD_SUBREDDIT_NAME = "GemTD";

  public static final String TELEGRAM_CHANNEL_NAME = "@gemtd_daily";
  public static final String REDDIT_SHOP_PREFIX = "[shop]";

  public static final String REDDIT_DOMAIN = "https://reddit.com";
  public static final String LINK_TEXT = "Check out reddit";
  public static final String DESCRITPION_TEXT = "Description";
  // delay of 12 hours
  public static final long DELAY_TIME_IN_MS = 1000 * 60 * 60 * 12;
  // check every 2 hours
  public static final long PERIOD_TIME_IN_MS = 1000 * 60 * 60 * 2;

}
