package de.cbl.home.telegram;

public class TelegramHelper {

  public static String link(String text, String url) {
    return "[" + text + "]" + "(" + url + ")";
  }

  public static String bold(String text) {
    return new String("**" + text + "**");
  }

  public static String code(String text) {
    return new String("```" + text + "```");
  }
}
